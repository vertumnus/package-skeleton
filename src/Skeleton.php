<?php

declare(strict_types=1);

namespace Vertumnus\Skeleton;

final class Skeleton
{
    public const PACKAGE_VENDOR = 'vertumnus';
    public const PACKAGE_NAME = 'skeleton';

    // Build your next great package.
}
