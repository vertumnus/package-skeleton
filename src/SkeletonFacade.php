<?php

declare(strict_types=1);

namespace Vertumnus\Skeleton;

use Illuminate\Support\Facades\Facade;

/**
 * @see Skeleton
 */
final class SkeletonFacade extends Facade
{
    /**
     * Get the registered name of the component.
     */
    protected static function getFacadeAccessor(): string
    {
        return Skeleton::PACKAGE_NAME;
    }
}
