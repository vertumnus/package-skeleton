<?php

declare(strict_types=1);

namespace Vertumnus\Skeleton;

use Illuminate\Support\ServiceProvider;

final class SkeletonServiceProvider extends ServiceProvider
{
    private const COMMANDS = [

    ];

    /**
     * Bootstrap the application services.
     */
    public function boot(): void
    {
        // Optional methods to load package assets
        //$this->loadMigrationsFrom($this->packageRoot() . '/database/migrations');
        //$this->loadRoutesFrom($this->packageRoot() . '/routes.php');
        //$this->loadTranslationsFrom($this->packageRoot() . '/resources/lang', Skeleton::PACKAGE_NAME);
        //$this->loadViewsFrom($this->packageRoot() . '/resources/views', Skeleton::PACKAGE_NAME);

        if ($this->app->runningInConsole()) {
            // Publish config
            $this->publishes([
                $this->packageRoot() . '/config/config.php' => config_path(Skeleton::PACKAGE_NAME . '.php'),
            ], Skeleton::PACKAGE_NAME . '-config');

            // Publish assets
            //$this->publishes([
            //    $this->packageRoot() . '/public' => public_path('vendor/' . Skeleton::PACKAGE_NAME),
            //], Skeleton::PACKAGE_NAME . '-assets');

            // Publish translation files
            //$this->publishes([
            //    $this->packageRoot() . '/resources/lang' => resource_path('lang/vendor/' . Skeleton::PACKAGE_NAME),
            //], Skeleton::PACKAGE_NAME . '-lang');

            // Publish views
            //$this->publishes([
            //    $this->packageRoot() . '/resources/views' => resource_path('views/vendor/' . Skeleton::PACKAGE_NAME),
            //], Skeleton::PACKAGE_NAME . '-views');

            // Registering package commands
            $this->commands(self::COMMANDS);
        }
    }

    /**
     * Register the application services.
     */
    public function register(): void
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom($this->packageRoot() . '/config/config.php', Skeleton::PACKAGE_NAME);

        // Register the main class to use with the facade
        $this->app->singleton(Skeleton::PACKAGE_NAME, static function () {
            return new Skeleton();
        });
    }

    private function packageRoot(): string
    {
        return dirname(__DIR__ . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR;
    }

    private function resourcesPath(): string
    {
        return $this->packageRoot() . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR;
    }
}
