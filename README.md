# Vertumnus Laravel package skeleton

[![Latest Version on Packagist](https://img.shields.io/packagist/v/vertumnus/skeleton.svg?style=flat-square)](https://packagist.org/packages/vertumnus/skeleton)
[![Total Downloads](https://img.shields.io/packagist/dt/vertumnus/skeleton.svg?style=flat-square)](https://packagist.org/packages/vertumnus/skeleton)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require vertumnus/skeleton
```

## Usage

```php
// Usage description here
```

### Testing
- Run PHPUnit: `composer unit`
- Run PHPLint: `composer lint`
- Run PHPCS/PHPCBF: `composer style`/`composer fix-style`
- Run require checker: `composer require-check`
- Run security checker: `composer security-check`
- Check for unused packages: `composer unused-check`
- **Run all tests: `composer test-all`**

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email security@gkcld.net instead of using the issue tracker.

## Authors
-   [Gertjan Krol](https://git.gertjankrol.nl/gertjan)

## License

The MIT License (MIT). Please see [LICENSE](LICENSE.md) for more information.
